%%script /usr/local/bin/mmix-script
N	IS	10
t	IS	$255
a	GREG	0
b	GREG	0
c	GREG	0
d	GREG	0
r	GREG	0

	LOC		Data_Segment
dtop	GREG	@
BUF	OCTA	0

	LOC	#100
Main	SET	a,0
	SET	b,1
	SET	r,N

Start	SET	c,b
	
0H	GREG	#202020202020200a
	STOU	0B,BUF
	LDA	t,BUF+6

1H	DIV	c,c,10
	GET	d,rR
	INCL	d,'0'
	STBU	d,t,0
	SUB	t,t,1
	PBNZ	c,1B
	LDA	t,BUF
	TRAP	0,Fputs,StdOut
	SUB	r,r,1

	ADD	c,a,b
	SET	a,b
	SET	b,c

	PBP	r,Start
	TRAP	0,Halt,0
