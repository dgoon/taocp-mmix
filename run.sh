#! /bin/sh
if [ $# -ne 1 ]; then
    echo "Usage: $0 [YOUR.mms]"
    exit -1
fi
TMP=`mktemp -t mmix-runner`
MMO="$TMP".mmo
./bin/mmixal -o $MMO $1
./bin/mmix $MMO
/bin/rm -f $MMO $TMP
